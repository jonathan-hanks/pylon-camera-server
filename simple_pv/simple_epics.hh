//
// Created by jonathan.hanks on 12/20/19.
//

#ifndef DAQD_TRUNK_SIMPLE_EPICS_HH
#define DAQD_TRUNK_SIMPLE_EPICS_HH

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <type_traits>
#include <gddAppFuncTable.h>
#include <epicsTimer.h>

#include "casdef.h"
#include "gddApps.h"
#include "smartGDDPointer.h"

#include "simple_pv.h"

namespace simple_epics
{
    namespace detail
    {
        class simplePVBase : public casPV
        {
        public:
            simplePVBase( ) : casPV( )
            {
            }
            ~simplePVBase( ) override = default;

            virtual void update( ) = 0;
        };
    } // namespace detail

    class Server;

    enum class PVMode
    {
        ReadOnly = 0,
        ReadWrite = 1,
    };

    /*!
     * @brief A description of a PV, used to describe an int PV to the server.
     * @note this is given a pointer to the data.  This value is only read
     * when a Server object is told to update its data.
     */
    template < typename IntType >
    class pvBasicIntAttributes
    {
        static_assert( std::is_integral< IntType >::value,
                       "integer type required" );

    public:
        using value_type = IntType;

        pvBasicIntAttributes( std::string                   pv_name,
                              IntType*                      value,
                              std::pair< IntType, IntType > alarm_range,
                              std::pair< IntType, IntType > warn_range,
                              PVMode mode = PVMode::ReadOnly )
            : name_{ std::move( pv_name ) },

              alarm_low_{ alarm_range.first },
              alarm_high_{ alarm_range.second }, warn_low_{ warn_range.first },
              warn_high_{ warn_range.second }, mode_{ mode }, src_{ value }
        {
        }

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

        IntType
        alarm_high( ) const noexcept
        {
            return alarm_high_;
        }
        IntType
        alarm_low( ) const noexcept
        {
            return alarm_low_;
        }
        IntType
        warn_high( ) const noexcept
        {
            return warn_high_;
        }
        IntType
        warn_low( ) const noexcept
        {
            return warn_low_;
        }

        const IntType*
        src( ) const noexcept
        {
            return src_;
        }

        PVMode
        mode( ) const noexcept
        {
            return mode_;
        }

    private:
        std::string name_;

        IntType alarm_high_;
        IntType alarm_low_;
        IntType warn_high_;
        IntType warn_low_;

        PVMode   mode_;
        IntType* src_{ nullptr };
    };
    using pvIntAttributes = pvBasicIntAttributes< std::int32_t >;
    using pvUIntAttributes = pvBasicIntAttributes< std::uint32_t >;
    using pvUShortAttributes = pvBasicIntAttributes< std::uint16_t >;
    static_assert( sizeof( std::int32_t ) == sizeof( int ),
                   "int must be 32 bit" );

    /*!
     * @brief A description of a PV, used to describe a string PV to the server.
     * @note this is given a pointer to the data.  This value is only read
     * when a Server object is told to update its data.
     */
    class pvStringAttributes
    {
    public:
        pvStringAttributes( std::string pv_name,
                            const char* value,
                            PVMode      mode = PVMode::ReadOnly,
                            std::size_t buffer_size = 0 )
            : name_{ std::move( pv_name ) }, mode_{ mode }, src_{ value },
              src_size_{ buffer_size }
        {
        }

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

        const char*
        src( ) const noexcept
        {
            return src_;
        }

        char*
        src( ) noexcept
        {
            return (char*)src_;
        }

        std::size_t
        src_size( ) const noexcept
        {
            return src_size_;
        }

        PVMode
        mode( ) const noexcept
        {
            return mode_;
        }

    private:
        std::string name_;
        PVMode      mode_;
        const char* src_;
        std::size_t src_size_;
    };

    /*!
     * @brief A description of a PV, used to describe a double PV to the server.
     * @note this is given a pointer to the data.  This value is only read
     * when a Server object is told to update its data.
     */
    class pvDoubleAttributes
    {
    public:
        pvDoubleAttributes( std::string                 pv_name,
                            double*                     value,
                            std::pair< double, double > alarm_range,
                            std::pair< double, double > warn_range,
                            PVMode mode = PVMode::ReadOnly )
            : name_{ std::move( pv_name ) },

              alarm_low_{ alarm_range.first },
              alarm_high_{ alarm_range.second }, warn_low_{ warn_range.first },
              warn_high_{ warn_range.second }, mode_{ mode }, src_{ value }
        {
        }

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

        double
        alarm_high( ) const noexcept
        {
            return alarm_high_;
        }
        double
        alarm_low( ) const noexcept
        {
            return alarm_low_;
        }
        double
        warn_high( ) const noexcept
        {
            return warn_high_;
        }
        double
        warn_low( ) const noexcept
        {
            return warn_low_;
        }

        const double*
        src( ) const noexcept
        {
            return src_;
        }

        PVMode
        mode( ) const noexcept
        {
            return mode_;
        }

    private:
        std::string name_;

        double alarm_high_;
        double alarm_low_;
        double warn_high_;
        double warn_low_;

        PVMode mode_;

        double* src_{ nullptr };
    };

    /*!
     * @brief An R/O implementation of the Portable CA Server.
     */
    class Server : public caServer
    {
    public:
        Server( ) : caServer( ), pvs_{ }
        {
        }
        ~Server( ) override;

        /*!
         * @brief Add a PV to the server.
         */
        void addPV( pvUShortAttributes attr );
        void addPV( pvIntAttributes attr );
        void addPV( pvUIntAttributes attr );
        void addPV( pvStringAttributes attr );
        void addPV( pvDoubleAttributes attr );

        /*!
         * @brief Reflect all changes in the data for each PV into the server
         */
        void update( );

        pvExistReturn pvExistTest( const casCtx&    ctx,
                                   const caNetAddr& clientAddress,
                                   const char*      pPVAliasName ) override;

        pvAttachReturn pvAttach( const casCtx& ctx,
                                 const char*   pPVAliasName ) override;

    private:
        std::mutex                                                       m_;
        std::map< std::string, std::unique_ptr< detail::simplePVBase > > pvs_;
    };

} // namespace simple_epics

#endif // DAQD_TRUNK_SIMPLE_EPICS_HH
