//
// Created by jonathan.hanks on 3/16/22.
//

#ifndef DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH
#define DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH

#include "simple_pv_types.hh"
#include "simple_pv_internal.hh"
#include <mutex>
#include <type_traits>

#include <iostream>

namespace simple_epics
{

    namespace detail
    {

        /*!
         * @brief A representation of a R/O integer in a PV
         */
        template < typename IntType >
        class simpleBasicIntPV : public simplePVBase
        {
            static_assert( std::is_integral< IntType >::value,
                           "Must use an interger type" );

        public:
            simpleBasicIntPV( caServer&                       server,
                              pvBasicIntAttributes< IntType > attr )
                : simplePVBase( ), server_{ server }, attr_{ std::move(
                                                          attr ) },
                  val_( ), monitored_{ false }
            {
                std::once_flag initted{ };
                std::call_once(
                    initted, []( ) { simpleBasicIntPV::setup_func_table( ); } );

                val_ = new gddScalar( gddAppType_value,
                                      ait_data_type< IntType >::value );
                val_->unreference( );
                set_value( *attr_.src( ) );
            }
            ~simpleBasicIntPV( ) override = default;

            caStatus
            read( const casCtx& ctx, gdd& prototype ) override
            {
                return get_func_table( ).read( *this, prototype );
            }
            caStatus
            write( const casCtx& ctx, const gdd& value ) override
            {
                if ( attr_.mode( ) != PVMode::ReadWrite )
                {
                    return S_casApp_noSupport;
                }
                aitType newValue;
                value.get( &newValue, ait_data_type< IntType >::value );
                set_value( newValue );
                *const_cast< IntType* >( attr_.src( ) ) = newValue;
                return S_casApp_success;
            }

            void destroy( ) override{ };

            aitEnum
            bestExternalType( ) const override
            {
                return val_->primitiveType( );
            }

            const char*
            getName( ) const override
            {
                return attr_.name( ).c_str( );
            }

            caStatus
            interestRegister( ) override
            {
                monitored_ = true;
                return S_casApp_success;
            }

            void
            interestDelete( ) override
            {
                monitored_ = false;
            }

            void
            update( ) override
            {
                set_value( *attr_.src( ) );
            }

        private:
            using aitType = typename ait_data_type< IntType >::ait_type;

            void
            set_value( aitType value )
            {
                aitType current_value = 0;

                val_->getConvert( current_value );
                if ( current_value == value )
                {
                    return;
                }

                val_->putConvert( value );
                aitTimeStamp ts = aitTimeStamp( epicsTime::getCurrent( ) );
                val_->setTimeStamp( &ts );

                aitUint16 stat = epicsAlarmNone;
                aitUint16 sevr = epicsSevNone;
                if ( value >= attr_.alarm_high( ) )
                {
                    stat = epicsAlarmHiHi;
                    sevr = epicsSevMajor;
                }
                else if ( value <= attr_.alarm_low( ) )
                {
                    stat = epicsAlarmLoLo;
                    sevr = epicsSevMajor;
                }
                else if ( value >= attr_.warn_high( ) )
                {
                    stat = epicsAlarmHigh;
                    sevr = epicsSevMinor;
                }
                else if ( value <= attr_.warn_low( ) )
                {
                    stat = epicsAlarmLow;
                    sevr = epicsSevMinor;
                }
                val_->setSevr( sevr );
                val_->setStat( stat );

                if ( monitored_ )
                {
                    casEventMask mask =
                        casEventMask( server_.valueEventMask( ) );
                    bool alarm_changed = ( stat != val_->getStat( ) ||
                                           sevr != val_->getSevr( ) );
                    if ( alarm_changed )
                    {
                        mask |= server_.alarmEventMask( );
                    }
                    postEvent( mask, *val_ );
                }
            }

            static void
            setup_func_table( )
            {
                auto install = []( const char* name,
                                   gddAppFuncTableStatus (
                                       simpleBasicIntPV::*handler )( gdd& ) ) {
                    gddAppFuncTableStatus status;

                    //           char error_string[100];

                    status = get_func_table( ).installReadFunc( name, handler );
                    if ( status != S_gddAppFuncTable_Success )
                    {
                        //                errSymLookup(status, error_string,
                        //                sizeof(error_string));
                        //               throw std::runtime_error(error_string);
                        throw std::runtime_error(
                            "Unable to initialize pv lookup table" );
                    }
                };

                install( "units", &simpleBasicIntPV::read_attr_not_handled );
                install( "status", &simpleBasicIntPV::read_status );
                install( "severity", &simpleBasicIntPV::read_severity );
                install( "maxElements",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "precision", &simpleBasicIntPV::read_precision );
                install( "alarmHigh", &simpleBasicIntPV::read_alarm_high );
                install( "alarmLow", &simpleBasicIntPV::read_alarm_low );
                install( "alarmHighWarning",
                         &simpleBasicIntPV::read_warn_high );
                install( "alarmLowWarning", &simpleBasicIntPV::read_warn_low );
                install( "maxElements",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "graphicHigh",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "graphicLow",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "controlHigh",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "controlLow",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "enums", &simpleBasicIntPV::read_attr_not_handled );
                install( "menuitem", &simpleBasicIntPV::read_attr_not_handled );
                install( "timestamp",
                         &simpleBasicIntPV::read_attr_not_handled );
                install( "value", &simpleBasicIntPV::read_value );
            }
            static gddAppFuncTable< simpleBasicIntPV >&
            get_func_table( )
            {
                static gddAppFuncTable< simpleBasicIntPV > func_table;
                return func_table;
            }

            gddAppFuncTableStatus
            read_attr_not_handled( gdd& g )
            {
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_status( gdd& g )
            {
                g.putConvert( val_->getStat( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_severity( gdd& g )
            {
                g.putConvert( val_->getSevr( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_precision( gdd& g )
            {
                g.putConvert( 0 );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_alarm_high( gdd& g )
            {
                g.putConvert( attr_.alarm_high( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_alarm_low( gdd& g )
            {
                g.putConvert( attr_.alarm_low( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_warn_high( gdd& g )
            {
                g.putConvert( attr_.warn_high( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_warn_low( gdd& g )
            {
                g.putConvert( attr_.warn_low( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_value( gdd& g )
            {
                auto status = gddApplicationTypeTable::app_table.smartCopy(
                    &g, val_.get( ) );
                return ( status ? S_cas_noConvert : S_casApp_success );
            }

            caServer&                       server_;
            pvBasicIntAttributes< IntType > attr_;
            smartGDDPointer                 val_;
            bool                            monitored_;
        };
        using simpleIntPV = simpleBasicIntPV< std::int32_t >;
        using simpleUIntPV = simpleBasicIntPV< std::uint32_t >;
        using simpleUShortPV = simpleBasicIntPV< std::uint16_t >;
    } // namespace detail
} // namespace simple_epics

#endif // DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH
