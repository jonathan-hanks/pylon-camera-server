//
// Created by jonathan.hanks on 3/16/22.
//

#ifndef DAQD_TRUNK_SIMPLE_PV_TYPES_HH
#define DAQD_TRUNK_SIMPLE_PV_TYPES_HH

#include <cstdint>
#include <string>
#include <type_traits>
#include "aitTypes.h"

namespace simple_epics
{
    namespace detail
    {
        /**
         * @brief ait_data_type is used to map between C++ types and the
         * CAS ait (architecture independent) data types.
         * @tparam T The type to map
         */
        template < typename T >
        struct ait_data_type
        {
            static const aitEnum value = aitEnumInvalid;
            using ait_type = void;
        };

        template <>
        struct ait_data_type< std::int16_t >
        {
            static const aitEnum value = aitEnumInt16;
            using ait_type = aitInt16;
            static_assert( std::is_same< std::int16_t, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< std::uint16_t >
        {
            static const aitEnum value = aitEnumUint16;
            using ait_type = aitUint16;
            static_assert( std::is_same< std::uint16_t, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< std::int32_t >
        {
            static const aitEnum value = aitEnumInt32;
            using ait_type = aitInt32;
            static_assert( std::is_same< std::int32_t, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< std::uint32_t >
        {
            static const aitEnum value = aitEnumUint32;
            using ait_type = aitUint32;
            static_assert( std::is_same< std::uint32_t, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< float >
        {
            static const aitEnum value = aitEnumFloat32;
            using ait_type = aitFloat32;
            static_assert( std::is_same< float, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< double >
        {
            static const aitEnum value = aitEnumFloat64;
            using ait_type = aitFloat64;
            static_assert( std::is_same< double, ait_type >::value,
                           "aitType and C++ types must match" );
        };

        template <>
        struct ait_data_type< const char* >
        {
            static const aitEnum value = aitEnumString;
        };

        template <>
        struct ait_data_type< std::string >
        {
            static const aitEnum value = aitEnumString;
        };
    } // namespace detail
} // namespace simple_epics

#endif // DAQD_TRUNK_SIMPLE_PV_TYPES_HH
