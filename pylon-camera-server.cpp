// Client: gst-launch-1.0 udpsrc port=5000 ! application/x-rtp ! rtph264depay ! queue ! avdec_h264 ! videoconvert ! autovideosink

#include <cmath>
#include <filesystem>
#include <limits>
#include <thread>
#include <mutex>

extern "C" {
  #include <gst/gst.h>
  #include <gst/video/video-frame.h>
}

#include <opencv2/opencv.hpp>

#include "simple_pv/simple_epics.hh"
#include "fdManager.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <pylon/PylonIncludes.h>
#include <pylon/BaslerUniversalInstantCamera.h>

#include <arpa/inet.h>


std::string channel_prefix;

double calc_noise_floor;
double calc_x;
double calc_y;
double calc_wx;
double calc_wy;
double calc_sum;

std::mutex image_mutex;
Pylon::CPylonImage image;
int32_t image_valid = false;

int64_t pixel_dynamic_range_max = std::numeric_limits<int32_t>::max();
int32_t saturated = 0;

GTimer* archive_timer;
int32_t archive_interval_minutes;
std::string archive_file_folder_path;
std::string archive_file_name_prefix;


struct gstreamer_data {
  guint source_id;

  GstElement* pylonsrc;
  GstElement* pylonsrc_capsfilter;
  GstElement* tee;

  GstElement* appsink_queue;
  GstElement* appsink;

/*
  GstElement* filesink_queue;
  GstElement* filesink_videoconvert;
  GstElement* filesink_avimux;
  GstElement* filesink;
*/

  GstElement* udpsink_queue;
  GstElement* udpsink_videoconvert;
  GstElement* udpsink_clockoverlay;
  GstElement* udpsink_x264enc;
  GstElement* udpsink_rtph264pay;
  GstElement* udpsink;

/*
  GstElement* autovideosink_queue;
  GstElement* autovideosink_videoconvert;
  GstElement* autovideosink;
*/

  GstElement* pipeline;

  GMainLoop* main_loop;
};


int32_t exposure_auto_req;
int32_t exposure_auto_set;
int32_t exposure_auto;

int32_t exposure_time_raw_req;
int32_t exposure_time_raw_set;
int32_t exposure_time_raw;

int32_t gain_auto_req;
int32_t gain_auto_set;
int32_t gain_auto;

int32_t gain_raw_req;
int32_t gain_raw_set;
int32_t gain_raw;

int32_t timeout_ms;

void process_epics() {
  simple_epics::Server server;


  server.addPV(
    simple_epics::pvDoubleAttributes(
      channel_prefix + std::string("X"),
      &calc_x,
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
    )
  );

  server.addPV(
    simple_epics::pvDoubleAttributes(
      channel_prefix + std::string("Y"),
      &calc_y,
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
    )
  );

  server.addPV(
    simple_epics::pvDoubleAttributes(
      channel_prefix + std::string("WX"),
      &calc_wx,
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
    )
  );

  server.addPV(
    simple_epics::pvDoubleAttributes(
      channel_prefix + std::string("WY"),
      &calc_wy,
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
    )
  );

  server.addPV(
    simple_epics::pvDoubleAttributes(
      channel_prefix + std::string("SUM"),
      &calc_sum,
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
      std::make_pair(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max())
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("VALID"),
      &image_valid,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("SATURATED"),
      &saturated,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP_AUTO_REQ"),
      &exposure_auto_req,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP_AUTO_SET"),
      &exposure_auto_set,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP_AUTO"),
      &exposure_auto,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );


  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP_REQ"),
      &exposure_time_raw_req,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP_SET"),
      &exposure_time_raw_set,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("EXP"),
      &exposure_time_raw,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN_AUTO_REQ"),
      &gain_auto_req,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN_AUTO_SET"),
      &gain_auto_set,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN_AUTO"),
      &gain_auto,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN_REQ"),
      &gain_raw_req,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN_SET"),
      &gain_raw_set,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("GAIN"),
      &gain_raw,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max())
    )
  );


  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("TIMEOUT"),
      &timeout_ms,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  server.addPV(
    simple_epics::pvIntAttributes(
      channel_prefix + std::string("ARCHIVE_INTERVAL"),
      &archive_interval_minutes,
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      std::make_pair(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
      simple_epics::PVMode::ReadWrite
    )
  );

  while (1) {
    // The EPICS server requires this to be periodically called.
    server.update();
    fileDescriptorManager.process(1);
  }
}


void process_pylon(Pylon::String_t serial_number, int64_t width, int64_t height, int64_t packet_size, int64_t inter_packet_delay) {
  try {
    Pylon::CBaslerUniversalInstantCamera camera;
    Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();
    Pylon::CDeviceInfo info;
    Pylon::DeviceInfoList_t filter;
    info.SetSerialNumber(serial_number);
    filter.push_back(info);
    Pylon::DeviceInfoList_t devices;

    while (1) {
      try {
        while (1) {
          if (tlFactory.EnumerateDevices(devices, filter) > 0) {
            g_print("The camera is connected.\n");
            break;
          }
          else {
            g_printerr("The camera is not connected.\n");
            Pylon::WaitObject::Sleep(5000);
          }
        }

        g_print("Attaching camera.\n");
        camera.Attach(tlFactory.CreateFirstDevice(info));

        g_print("Opening camera.\n");
        camera.Open();

        g_print("Device Model Name: %s\n", camera.DeviceModelName.GetValue().c_str());

        g_print("Device ID: %s\n", camera.DeviceID.GetValue().c_str());

        g_print("Device User ID: %s\n", camera.DeviceUserID.GetValue().c_str());

        struct in_addr ip_addr;
        ip_addr.s_addr = htonl(camera.GevCurrentIPAddress.GetValue());
        g_print("Current IP Address: %s\n", inet_ntoa(ip_addr));

        g_print("Width: %i\n", camera.Width.GetValue());

        g_print("Height: %i\n", camera.Height.GetValue());

        g_print("Setting GevSCPSPacketSize (packet size): %li\n", packet_size);
        camera.GevSCPSPacketSize.TrySetValue(packet_size);

        g_print("Setting GevSCPD (inter-packet delay): %li\n", inter_packet_delay);
        camera.GevSCPD.TrySetValue(inter_packet_delay);

        pixel_dynamic_range_max = camera.PixelDynamicRangeMax.GetValue();

        /*
        camera.ChunkModeActive.SetValue(true);
        camera.ChunkSelector.SetValue(ChunkSelector_ChunkTimestamp);
        camera.ChunkEnable.SetValue(true);
        */

        g_print("Starting grabbing.\n");
        camera.StartGrabbing(Pylon::GrabStrategy_LatestImageOnly);

        Pylon::CGrabResultPtr ptrGrabResult;

        exposure_auto_set = 1;
        exposure_time_raw_set = 1;
        gain_auto_set = 1;
        gain_raw_set = 1;

        while (1) {
          // Exposure Time

          int32_t epics_exposure_auto_req = exposure_auto_req;
          int32_t epics_exposure_auto_set = exposure_auto_set;

          if (epics_exposure_auto_set > 0) {
            g_print("Setting auto exposure: %i\n", epics_exposure_auto_req);
            camera.ExposureAuto.SetIntValue((int64_t) epics_exposure_auto_req);
            exposure_auto_set = 0;
          }
          exposure_auto = camera.ExposureAuto.GetIntValue();


          int32_t epics_exposure_time_raw_req = exposure_time_raw_req;
          int32_t epics_exposure_time_raw_set = exposure_time_raw_set;

          if (epics_exposure_time_raw_set > 0) {
            g_print("Setting exposure time: %i\n", epics_exposure_time_raw_req);
            camera.ExposureTimeRaw.TrySetValue((int64_t) epics_exposure_time_raw_req, Pylon::IntegerValueCorrection_Nearest);
            exposure_time_raw_set = 0;
          }
          exposure_time_raw = camera.ExposureTimeRaw.GetValue();


          // Gain

          int32_t epics_gain_auto_req = gain_auto_req;
          int32_t epics_gain_auto_set = gain_auto_set;

          if (epics_gain_auto_set > 0) {
            g_print("Setting auto gain: %i\n", epics_gain_auto_req);
            camera.GainAuto.SetIntValue((int64_t) epics_gain_auto_req);
            gain_auto_set = 0;
          }
          gain_auto = camera.GainAuto.GetIntValue();


          int32_t epics_gain_raw_req = gain_raw_req;
          int32_t epics_gain_raw_set = gain_raw_set;

          if (epics_gain_raw_set > 0) {
            g_print("Setting gain: %i\n", epics_gain_raw_req);
            camera.GainRaw.TrySetValue((int64_t) epics_gain_raw_req, Pylon::IntegerValueCorrection_Nearest);
            gain_raw_set = 0;
          }
          gain_raw = camera.GainRaw.GetValue();


          try {
            camera.RetrieveResult((unsigned int) timeout_ms, ptrGrabResult, Pylon::TimeoutHandling_ThrowException);
            if (ptrGrabResult->GrabSucceeded()) {
              image_valid = true;
              if (image_mutex.try_lock()) {
                image.AttachGrabResultBuffer(ptrGrabResult);
                image_mutex.unlock();
              }
            }
            else {
              image_valid = false;
              g_printerr("The grab failed.\n");
              g_printerr("%s\n", ptrGrabResult->GetErrorDescription().c_str());
            }
          }
          catch (const Pylon::TimeoutException e) {
            image_valid = false;
            g_printerr("%s\n", e.GetDescription());
          }
        }
      }
      catch (const Pylon::GenericException& e) {
        image_valid = false;
        Pylon::WaitObject::Sleep(1000);

        if (camera.IsCameraDeviceRemoved()) {
          g_printerr("The connection to the camera has been lost.\n");
          camera.DestroyDevice();
        }
        else {
          g_printerr("%s\n", e.GetDescription());
        }
      }
    }
  }
  catch (const Pylon::GenericException& e) {
    image_valid = false;
    g_printerr("An exception occurred.\n");
    g_printerr("%s\n", e.GetDescription());
    exit(-1);
  }
}


static gboolean push_data(gstreamer_data* data) {
  if (image_mutex.try_lock()) {
    if (image.IsValid()) {
      //const uint8_t* pImageBuffer = (uint8_t*) image.GetBuffer();
      //printf("Gray value of first pixel: %u\n", (uint32_t) pImageBuffer[0]);

      GstBuffer* buffer = gst_buffer_new_wrapped_full( // transfer full
        (GstMemoryFlags) GST_MEMORY_FLAG_PHYSICALLY_CONTIGUOUS,
        (gpointer) image.GetBuffer(),
        image.GetImageSize(),
        0,
        image.GetImageSize(),
        NULL,
        NULL
      );

      const guint n_planes = 1;
      size_t padding_x = image.GetPaddingX();
      gsize offset[n_planes] = {padding_x};

      size_t stride_scalar;
      image.GetStride(stride_scalar);
      gint stride[n_planes] = {(gint) stride_scalar};

      gst_buffer_add_video_meta_full ( // transfer none
        buffer,
        GST_VIDEO_FRAME_FLAG_NONE,
        GST_VIDEO_FORMAT_GRAY8,
        image.GetWidth(),
        image.GetHeight(),
        n_planes,
        offset,
        stride
      );

      GstFlowReturn ret;
      g_signal_emit_by_name(data->pylonsrc, "push-buffer", buffer, &ret);
      if (ret != GST_FLOW_OK) {
        g_printerr("Error\n");
        gst_buffer_unref(buffer);
        image.Release();
        image_mutex.unlock();
        return false;
      }

      gst_buffer_unref(buffer);
      image.Release();
    }
    image_mutex.unlock();
  }

  usleep(1);

  return true;
}

static void start_feed(GstElement* source, guint size, gstreamer_data* data) {
  (void) source;
  (void) size;


  if (data->source_id == 0) {
    //g_print("Start feeding\n");
    data->source_id = g_idle_add((GSourceFunc) push_data, data);
  }
}

static void stop_feed(GstElement* source, gstreamer_data* data) {
  (void) source;


  if (data->source_id != 0) {
    //g_print("Stop feeding\n");
    g_source_remove(data->source_id);
    data->source_id = 0;
  }
}


gboolean bus_callback(GstBus* bus, GstMessage* message, gpointer user_data) {
  (void) bus;
  GMainLoop* main_loop = ((gstreamer_data*) user_data)->main_loop;

  switch (GST_MESSAGE_TYPE(message)) {
    case GST_MESSAGE_EOS: {
      g_print("End of stream\n");
      g_main_loop_quit(main_loop);
      break;
    }
    case GST_MESSAGE_ERROR: {
      GError* error;
      gchar* debug;
      gst_message_parse_error(message, &error, &debug);
      g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(message->src), error->message);
      g_error_free(error);
      g_printerr("Debugging information: %s\n", debug ? debug : "none");
      g_free(debug);
      g_main_loop_quit(main_loop);
      break;
    }
    case GST_MESSAGE_WARNING: {
      GError* error;
      gchar* debug;
      gst_message_parse_warning(message, &error, &debug);
      g_printerr("Warning received from element %s: %s\n", GST_OBJECT_NAME(message->src), error->message);
      g_error_free(error);
      g_printerr("Debugging information: %s\n", debug ? debug : "none");
      g_free(debug);
      break;
    }
    default:
      //g_print("%s %s\n", GST_MESSAGE_SRC_NAME(message), GST_MESSAGE_TYPE_NAME(message));
      break;
  }

  return TRUE;
}

static GstFlowReturn new_sample_callback(GstElement* appsink, gpointer udata) {
  (void) udata;

  GDateTime* current_date_time = g_date_time_new_now_utc(); // transfer full

  GstSample* sample;
  g_signal_emit_by_name(appsink, "pull-sample", &sample);
  if (sample) {
    GstCaps* caps = gst_sample_get_caps(sample); // transfer none
    if (caps) {
      GstBuffer* buffer = gst_sample_get_buffer(sample); // transfer none
      if (buffer) {
        GstMapInfo map_info;
        if (gst_buffer_map(buffer, &map_info, GST_MAP_READ)) {
          GstVideoInfo* video_info = gst_video_info_new(); // transfer full
          if (gst_video_info_from_caps(video_info, caps)) {
            //g_print("%s %i %i\n", video_info->finfo->name, GST_VIDEO_INFO_WIDTH(video_info), GST_VIDEO_INFO_HEIGHT(video_info));

            cv::Mat frame = cv::Mat(cv::Size(GST_VIDEO_INFO_WIDTH(video_info), GST_VIDEO_INFO_HEIGHT(video_info)),
            CV_8U, (char*) map_info.data, cv::Mat::AUTO_STEP);

            cv::Mat thresh_frame;
            cv::threshold(frame, thresh_frame, calc_noise_floor, 255, cv::THRESH_TOZERO);

            double minVal;
            double maxVal;
            cv::Point minLoc;
            cv::Point maxLoc;
            cv::minMaxLoc(thresh_frame, &minVal, &maxVal, &minLoc, &maxLoc);
            if (maxVal >= (double) pixel_dynamic_range_max) {
              saturated = 1;
            }
            else {
              saturated = 0;
            }

            cv::Moments m = cv::moments(thresh_frame, false);

            double calc_x_tmp = m.m10 / m.m00;
            double calc_y_tmp = m.m01 / m.m00;

            double calc_wx_tmp = 2.0 * sqrt(m.m20 / m.m00 - pow(calc_x_tmp, 2.0));
            double calc_wy_tmp = 2.0 * sqrt(m.m02 / m.m00 - pow(calc_y_tmp, 2.0));

            calc_sum = cv::sum(cv::sum(thresh_frame))[0];

            if (isfinite(calc_x_tmp)) {
              calc_x = calc_x_tmp;
            }
            else {
              calc_x = -1;
            }

            if (isfinite(calc_y_tmp)) {
              calc_y = calc_y_tmp;
            }
            else {
              calc_y = -1;
            }

            if (isfinite(calc_wx_tmp)) {
              calc_wx = calc_wx_tmp;
            }
            else {
              calc_wx = -1;
            }

            if (isfinite(calc_wy_tmp)) {
              calc_wy = calc_wy_tmp;
            }
            else {
              calc_wy = -1;
            }


            if (archive_interval_minutes > 0) {
              gdouble elapsed = g_timer_elapsed(archive_timer, NULL);
              if (elapsed / (gdouble) 60.0 >= (gdouble) archive_interval_minutes) {
                g_timer_reset(archive_timer);

                gchar* current_date_time_str = g_date_time_format(current_date_time, "%Y-%m-%d-%H-%M-%S"); // transfer full
                archive_file_folder_path += std::string(g_date_time_format(current_date_time, "%Y/%m/%d/"));
                std::filesystem::create_directories(archive_file_folder_path);
                std::string filename = archive_file_folder_path + archive_file_name_prefix + std::string("_") + std::string(current_date_time_str) + std::string(".tif");
                cv::imwrite(filename.c_str(), frame);
                g_free(current_date_time_str);
              }
            }
          }
          gst_video_info_free(video_info);
          gst_buffer_unmap(buffer, &map_info);
        }
      }
    }
    gst_sample_unref(sample);
  }

  g_date_time_unref(current_date_time);

  return GST_FLOW_OK;
}

static GstFlowReturn overrun_callback(GstElement* queue, gpointer udata) {
  g_printerr("%s overrun\n", queue->object.name);

  return GST_FLOW_OK;
}

int main(int argc, char *argv[]) {
  std::string prog_name = "pylon-camera-server";

  std::string config_file_path;
  if (argc == 2) {
    config_file_path = argv[1];
  }
  else {
    g_print("Version: 0.1.13\n");
    g_print("Usage: %s FILE\n", prog_name.c_str());
    g_print("FILE: The path to an ini configuration file\n");
    return -1;
  }

  boost::property_tree::ptree pt;

  std::string serial_number;
  int64_t packet_size;
  int64_t inter_packet_delay;

  int width;
  int height;
  int framerate_numerator;
  int framerate_denominator;
  std::string format;

  std::string overlay_text;
  bool overlay_silent;

  std::string host;
  int port;
  int ttl_mc;

  try {
    read_ini(config_file_path, pt);

    serial_number = pt.get<std::string>("serial_number");
    packet_size = pt.get<int64_t>("packet_size", 8192);
    inter_packet_delay = pt.get<int64_t>("inter_packet_delay", 50000);

    exposure_auto_req = pt.get<int32_t>("exposure_auto", 0);
    exposure_time_raw_req = pt.get<int32_t>("exposure_time_raw", 3000);

    gain_auto_req = pt.get<int32_t>("gain_auto", 0);
    gain_raw_req = pt.get<int32_t>("gain_raw", 0);

    timeout_ms = pt.get<unsigned int>("timeout_ms", 5000);

    width = pt.get<int>("width");
    height = pt.get<int>("height");
    framerate_numerator = pt.get<int>("framerate_numerator", 25);
    framerate_denominator = pt.get<int>("framerate_denominator", 1);
    format = pt.get<std::string>("format", std::string("GRAY8"));

    overlay_text = pt.get<std::string>("overlay_text");
    overlay_silent = pt.get<bool>("overlay_silent", false);

    host = pt.get<std::string>("multicast_address");
    port = pt.get<int>("multicast_port", 5000);
    ttl_mc = pt.get<int>("ttl_mc", 64);

    channel_prefix = pt.get<std::string>("channel_prefix");

    calc_noise_floor = pt.get<double>("calc_noise_floor", 25.0);

    archive_interval_minutes = pt.get<int32_t>("archive_interval_minutes", 0);
    archive_file_folder_path = pt.get<std::string>("archive_file_folder_path");
    archive_file_name_prefix = pt.get<std::string>("archive_file_name_prefix");
  }
  catch (const boost::property_tree::ptree_error &e) {
    g_printerr("An exception occurred parsing the configuration file.\n");
    g_printerr("%s\n", e.what());
    return -1;
  }


  archive_timer = g_timer_new();

  gst_init(NULL, NULL);

  Pylon::PylonAutoInitTerm autoInitTerm;

  gstreamer_data data;


  data.pylonsrc = gst_element_factory_make("appsrc", "pylonsrc");
  if (!data.pylonsrc) {
    g_printerr("Failed to create element 'pylonsrc'\n");
    return -1;
  }

  data.pylonsrc_capsfilter = gst_element_factory_make("capsfilter", "pylonsrc_capsfilter");
  if (!data.pylonsrc_capsfilter) {
    g_printerr("Failed to create element 'pylonsrc_capsfilter'\n");
    return -1;
  }

  data.tee = gst_element_factory_make("tee", "tee");
  if (!data.tee) {
    g_printerr("Failed to create element 'tee'\n");
    return -1;
  }


  data.appsink_queue = gst_element_factory_make("queue", "appsink_queue");
  if (!data.appsink_queue) {
    g_printerr("Failed to create element 'appsink_queue'\n");
    return -1;
  }

  data.appsink = gst_element_factory_make("appsink", "appsink");
  if (!data.appsink) {
    g_printerr("Failed to create element 'appsink'\n");
    return -1;
  }

/*
  data.filesink_queue = gst_element_factory_make("queue", "filesink_queue");
  if (!data.filesink_queue) {
    g_printerr("Failed to create element 'filesink_queue'\n");
    return -1;
  }

  data.filesink_videoconvert = gst_element_factory_make("videoconvert", "filesink_videoconvert");
  if (!data.filesink_videoconvert) {
    g_printerr("Failed to create element 'filesink_videoconvert'\n");
    return -1;
  }

  data.filesink_avimux = gst_element_factory_make("avimux", "filesink_avimux");
  if (!data.filesink_avimux) {
    g_printerr("Failed to create element 'filesink_avimux'\n");
    return -1;
  }

  data.filesink = gst_element_factory_make("filesink", "filesink");
  if (!data.filesink) {
    g_printerr("Failed to create element 'filesink'\n");
    return -1;
  }
*/

  data.udpsink_queue = gst_element_factory_make("queue", "udpsink_queue");
  if (!data.udpsink_queue) {
    g_printerr("Failed to create element 'udpsink_queue'\n");
    return -1;
  }

  data.udpsink_videoconvert = gst_element_factory_make("videoconvert", "udpsink_videoconvert");
  if (!data.udpsink_videoconvert) {
    g_printerr("Failed to create element 'udpsink_videoconvert'\n");
    return -1;
  }

  data.udpsink_clockoverlay = gst_element_factory_make("clockoverlay", "udpsink_clockoverlay");
  if (!data.udpsink_clockoverlay) {
    g_printerr("Failed to create element 'udpsink_clockoverlay'\n");
    return -1;
  }

  data.udpsink_x264enc = gst_element_factory_make("x264enc", "udpsink_x264enc");
  if (!data.udpsink_x264enc) {
    g_printerr("Failed to create element 'udpsink_x264enc'\n");
    return -1;
  }

  data.udpsink_rtph264pay = gst_element_factory_make("rtph264pay", "udpsink_rtph264pay");
  if (!data.udpsink_rtph264pay) {
    g_printerr("Failed to create element 'udpsink_rtph264pay'\n");
    return -1;
  }

  data.udpsink = gst_element_factory_make("udpsink", "udpsink");
  if (!data.udpsink) {
    g_printerr("Failed to create element 'udpsink'\n");
    return -1;
  }

/*
  data.autovideosink_queue = gst_element_factory_make("queue", "autovideosink_queue");
  if (!data.autovideosink_queue) {
    g_printerr("Failed to create element 'autovideosink_queue'\n");
    return -1;
  }

  data.autovideosink_videoconvert = gst_element_factory_make("videoconvert", "autovideosink_videoconvert");
  if (!data.udpsink_videoconvert) {
    g_printerr("Failed to create element 'autovideosink_videoconvert'\n");
    return -1;
  }

  data.autovideosink = gst_element_factory_make("autovideosink", "autovideosink");
  if (!data.autovideosink) {
    g_printerr("Failed to create element 'autovideosink'\n");
    return -1;
  }
*/

  // This had to be explicitly set in order to get the 'start_feed' and 'stop_feed' functions to be called more than once.
  data.source_id = 0;

  g_object_set(G_OBJECT(data.pylonsrc),
    "stream-type", 0, // GST_APP_STREAM_TYPE_STREAM,
    "format", GST_FORMAT_TIME,
    "is-live", TRUE,
    "do-timestamp", TRUE, // needed for the pipeline to run
    NULL
  );

  g_signal_connect(data.pylonsrc, "need-data", G_CALLBACK(start_feed), &data);
  g_signal_connect(data.pylonsrc, "enough-data", G_CALLBACK(stop_feed), &data);

  g_object_set(G_OBJECT(data.pylonsrc_capsfilter),
    "caps",
      gst_caps_new_simple ("video/x-raw",
        "format", G_TYPE_STRING, format.c_str(),
        "framerate", GST_TYPE_FRACTION, framerate_numerator, framerate_denominator,
        "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
        "width", G_TYPE_INT, width,
        "height", G_TYPE_INT, height,
        NULL
    ),
    NULL
  );

  g_object_set(
    G_OBJECT(data.udpsink_queue),
    "leaky", 2,
    "max-size-buffers", 1,
    "max-size-bytes", 0,
    "max-size-time", 0,
    NULL
  );

  g_object_set(
    G_OBJECT(data.appsink_queue),
    "leaky", 2,
    "max-size-buffers", 1,
    "max-size-bytes", 0,
    "max-size-time", 0,
    NULL
  );

  g_object_set(
    G_OBJECT(data.appsink),
    "drop", TRUE,
    "emit-signals", TRUE,
    "max-buffers", 1,
    NULL
  );

  g_signal_connect(data.appsink, "new-sample", G_CALLBACK(new_sample_callback), NULL);

/*
  g_object_set(
    G_OBJECT(data.filesink),
    "location", "test.avi",
    NULL
  );
*/

  g_object_set(
    G_OBJECT(data.udpsink_clockoverlay),
    "draw-outline", FALSE,
    "draw-shadow", TRUE,
    "text", (overlay_text + '\n').c_str(),
    "halignment", 0, // left
    "valignment", 2, // top
    "font-desc", "Courier, 18",
    "silent", overlay_silent,
    "time-format", "%Y-%m-%d %H:%M:%S %Z",
    NULL
  );

  g_object_set(
    G_OBJECT(data.udpsink_x264enc),
    "pass", 5, // qual
    "quantizer", 20,
    "tune", 0x00000004, // zerolatency
    NULL
  );

  g_object_set(
    G_OBJECT(data.udpsink),
    "host", host.c_str(),
    "port", port,
    "ttl-mc", ttl_mc,
    "sync", FALSE,
    NULL
  );

  g_signal_connect(data.appsink_queue, "overrun", G_CALLBACK(overrun_callback), NULL);

  g_signal_connect(data.udpsink_queue, "overrun", G_CALLBACK(overrun_callback), NULL);


  data.pipeline = gst_pipeline_new("pipeline");
  if (!data.pipeline) {
    g_printerr("Failed to create pipeline\n");
    return -1;
  }

  gst_bin_add_many(
    GST_BIN(data.pipeline),
    data.pylonsrc,
    data.pylonsrc_capsfilter,
    data.tee,
    data.appsink_queue,
    data.appsink,
/*
    data.filesink_queue,
    data.filesink_videoconvert,
    data.filesink_avimux,
    data.filesink,
*/
    data.udpsink_queue,
    data.udpsink_videoconvert,
    data.udpsink_clockoverlay,
    data.udpsink_x264enc,
    data.udpsink_rtph264pay,
    data.udpsink,
    //data.autovideosink_queue,
    //data.autovideosink_videoconvert,
    //data.autovideosink,
    NULL
  );

  if (
    gst_element_link_many(
      data.pylonsrc,
      data.pylonsrc_capsfilter,
      data.tee,
      NULL
    ) != TRUE
  )
  {
    g_printerr("pylonsrc elements could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }

  if (
    gst_element_link_many(
      data.appsink_queue,
      data.appsink,
      NULL
    ) != TRUE
  )
  {
    g_printerr("appsink elements could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }

/*
  if (
    gst_element_link_many(
      data.filesink_queue,
      data.filesink_videoconvert,
      data.filesink_avimux,
      data.filesink,
      NULL
    ) != TRUE
  )
  {
    g_printerr("filesink elements could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }
*/

  if (
    gst_element_link_many(
      data.udpsink_queue,
      data.udpsink_videoconvert,
      data.udpsink_clockoverlay,
      data.udpsink_x264enc,
      data.udpsink_rtph264pay,
      data.udpsink,
      NULL
    ) != TRUE
  )
  {
    g_printerr("udpsink elements could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }
/*
  if (
    gst_element_link_many(
      data.autovideosink_queue,
      data.autovideosink_videoconvert,
      data.autovideosink,
      NULL
    ) != TRUE
  )
  {
    g_printerr("autovideosink elements could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }
*/
  GstPadTemplate* pad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(data.tee), "src_%u"); // transfer none

  GstPad* tee_appsink_pad = gst_element_request_pad(data.tee, pad_template, NULL, NULL); // transfer full
  g_print("Obtained request pad %s for appsink branch.\n", gst_pad_get_name(tee_appsink_pad));
  GstPad* appsink_queue_pad = gst_element_get_static_pad(data.appsink_queue, "sink"); // transfer full
/*
  GstPad* tee_filesink_pad = gst_element_request_pad(data.tee, pad_template, NULL, NULL);
  g_print("Obtained request pad %s for filesink branch.\n", gst_pad_get_name(tee_filesink_pad));
  GstPad* filesink_queue_pad = gst_element_get_static_pad(data.filesink_queue, "sink");
*/
  GstPad* tee_udpsink_pad = gst_element_request_pad(data.tee, pad_template, NULL, NULL); // transfer full
  g_print("Obtained request pad %s for udpsink branch.\n", gst_pad_get_name(tee_udpsink_pad));
  GstPad* udpsink_queue_pad = gst_element_get_static_pad(data.udpsink_queue, "sink"); // transfer full
/*
  GstPad* tee_autovideosink_pad = gst_element_request_pad(data.tee, pad_template, NULL, NULL);
  g_print("Obtained request pad %s for autovideosink branch.\n", gst_pad_get_name(tee_autovideosink_pad));
  GstPad* autovideosink_queue_pad = gst_element_get_static_pad(data.autovideosink_queue, "sink");
*/
  if (
    gst_pad_link(tee_appsink_pad, appsink_queue_pad) != GST_PAD_LINK_OK ||
    //gst_pad_link(tee_filesink_pad, filesink_queue_pad) != GST_PAD_LINK_OK ||
    gst_pad_link(tee_udpsink_pad, udpsink_queue_pad) != GST_PAD_LINK_OK
    //gst_pad_link(tee_autovideosink_pad, autovideosink_queue_pad) != GST_PAD_LINK_OK
  )
  {
    g_printerr("Tee could not be linked.\n");
    gst_object_unref(data.pipeline);
    return -1;
  }
  gst_object_unref(tee_appsink_pad);
  gst_object_unref(appsink_queue_pad);
  //gst_object_unref(filesink_queue_pad);
  gst_object_unref(tee_udpsink_pad);
  gst_object_unref(udpsink_queue_pad);
  //gst_object_unref(autovideosink_queue_pad);


  data.main_loop = g_main_loop_new(NULL, FALSE);

  GstBus* bus = gst_element_get_bus(data.pipeline); // transfer full
  guint bus_watch_id = gst_bus_add_watch(bus, (GstBusFunc) bus_callback, &data);
  gst_object_unref(bus);

  GstStateChangeReturn ret = gst_element_set_state(data.pipeline, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Unable to set the pipeline to the playing state.\n");
    gst_object_unref(data.pipeline);
    g_source_remove(bus_watch_id);
    g_main_loop_unref(data.main_loop);
    return -1;
  }

  std::thread epics_thread(process_epics);
  std::thread pylon_thread(process_pylon, serial_number.c_str(), (int64_t) width, (int64_t) height, packet_size, inter_packet_delay);


  g_print("Starting main loop.\n");

  g_main_loop_run(data.main_loop);

  epics_thread.join();
  pylon_thread.join();


  gst_element_set_state(data.pipeline, GST_STATE_NULL);
  gst_object_unref(data.pipeline);
  g_source_remove(bus_watch_id);
  g_main_loop_unref(data.main_loop);

  g_timer_destroy(archive_timer);

  return 0;
}
